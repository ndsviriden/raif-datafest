import sys
import os
import yaml
from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime
from datetime import timedelta

this_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(this_dir)

from stages import statistics, fit, predict

# Load configs
config_path = os.path.join(this_dir, 'config.yml')
with open(config_path, 'r') as stream:
    config = yaml.safe_load(stream)

# Set dag name
branch_name = config["branch_name"]
dag_name = f'{branch_name}_consumption'

# Set dag args
default_dag_args = {
    'owner': 'airflow',
    'depends_on_past': True,
    'start_date': datetime(2013, 1, 1),
    'end_date': datetime(2017, 12, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=5)}

with DAG(dag_name,
         default_args=default_dag_args,
         schedule_interval='@monthly',
         max_active_runs=1,
         params={"config": config}) as dag:

    train_stage = PythonOperator(task_id='train_stage', python_callable=fit, provide_context=True)
    predict_stage = PythonOperator(task_id='predict_stage', python_callable=predict, provide_context=True)
    stat_stage = PythonOperator(task_id='stat_stage', python_callable=statistics, provide_context=True)

    train_stage >> predict_stage >> stat_stage
