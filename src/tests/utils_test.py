import unittest
from utils.date_utils import one_month_back, one_year_back


class TestDateUtils(unittest.TestCase):

    def test_month_back(self):
        day_1 = "2021-03"
        date_1 = one_month_back(date=day_1)
        self.assertEqual(date_1, "2021-02")
        day_2 = "2030-01"
        date_2 = one_month_back(date=day_2)
        self.assertEqual(date_2, "2029-12")

    def test_year_back(self):
        day_1 = "2021-03"
        date_1 = one_year_back(date=day_1)
        self.assertEqual(date_1, "2020-03")
        day_2 = "2030-01"
        date_2 = one_year_back(date=day_2)
        self.assertEqual(date_2, "2029-01")


if __name__ == '__main__':
    unittest.main()
