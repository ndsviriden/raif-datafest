import pandas as pd
from sqlalchemy import create_engine, text


class PostgresConnector:
    def __init__(self,
                 config: dict,
                 ):
        pg_info = config['postgres']
        host = pg_info['host']
        port = pg_info['port']
        user = pg_info['user']
        password = pg_info['password']
        database = pg_info['database']
        self.host = host
        self.port = port
        self.engine = create_engine(f'postgres+psycopg2://{user}:{password}@{host}:{port}/{database}')

    def get_target(self, start_date, end_date):
        target = pd.read_sql_query(
            f"select year_month, consumption "
            f"from target where year_month between "
            f"'{start_date}' and '{end_date}';", con=self.engine)
        return target

    def get_features(self, start_date, end_date):

        features = pd.read_sql_query(f"select year_month, solar, wind from features "
                                     f"where year_month between '{start_date}' and '{end_date}';", con=self.engine)

        return features

    def get_train(self, start_date, end_date):

        features = pd.read_sql_query(f"select features.year_month, target.consumption, features.solar, features.wind "
                                     f"from features inner join target "
                                     f"on features.year_month = target.year_month "
                                     f"where target.year_month between '{start_date}' and '{end_date}';", con=self.engine)

        return features[["wind", "solar"]], features["consumption"]

    def get_predicts(self, start_date, end_date, branch_name):
        target = pd.read_sql_query(
            f"select year_month, predict "
            f"from score where branch='{branch_name}' "
            f"and year_month between "
            f"'{start_date}' and '{end_date}';", con=self.engine)
        return target

    def write_predicts(self, date, branch_name, predict):

        query = text(f"INSERT INTO score(year_month, branch, predict) "
                     f"VALUES ('{date}', '{branch_name}', {predict}) "
                     f"ON CONFLICT (year_month, branch) "
                     f"DO UPDATE SET predict= excluded.predict")

        self.engine.execute(query)
