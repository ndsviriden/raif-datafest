from datetime import datetime
from datetime import timedelta


def one_month_back(date: str):
    """
    Here we assume, that data in ISO 8601 format.
    :param date: iso8601 string in YYYY-MM format;
    :return: iso8601 in YYYY-MM format.
    """
    date = datetime.strptime(date, '%Y-%m')
    # Because timedelta cannot handle month.
    date = date - timedelta(days=28)
    return date.strftime('%Y-%m')


def one_year_back(date: str):
    """
    Here we assume, that data in ISO 8601 format.
    :param date: iso8601 string in YYYY-MM format.
    :return: iso8601 in YYYY-MM format.
    """
    date = datetime.strptime(date, '%Y-%m')
    # Because timedelta cannot handle year.
    date = date - timedelta(days=364)
    return date.strftime('%Y-%m')
