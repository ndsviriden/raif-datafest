import logging
from influxdb import InfluxDBClient


class InfluxConnector:
    def __init__(self,
                 config: dict,
                 ):
        host = config['influx']['host']
        port = config['influx']['port']
        database = config['influx']['database']
        self.client = InfluxDBClient(host=host, port=port)
        self.client.switch_database(database)

    def _write_point(self, ts, measurement, tags, values):
        metric_json = {
            'measurement': measurement,
            'tags': tags,
            'fields': values,
            'time': ts
             }
        self.client.write_points([metric_json])

    def write_metric_point(self, metrics_value, ts, branch_name, metric_name='diff'):
        meas = 'metrics'
        tags = {'metric_name': metric_name, 'branch_name': branch_name}
        values = {'metric': metrics_value}
        self._write_point(ts=ts, measurement=meas, values=values, tags=tags)

    def write_score_point(self, score_value, ts, branch_name, model_name='lr'):
        meas = 'scores'
        tags = {'model_name': model_name, 'branch_name': branch_name}
        values = {'score': score_value}
        self._write_point(ts=ts, measurement=meas, values=values, tags=tags)

    def write_target_point(self, target_value, ts, branch_name, target_name='consumption'):
        meas = 'target'
        tags = {'target': target_name, 'branch_name': branch_name}
        values = {'target': target_value}
        self._write_point(ts=ts, measurement=meas, values=values, tags=tags)


class InfluxHandler(logging.Handler):
    def __init__(self,
                 config: dict,
                 measurement: str = "logs") -> None:
        self.measurement = measurement
        self.connector = InfluxConnector(config=config)
        super().__init__()

    def emit(self, record):
        log_json = {
            "measurement": self.measurement,
            "tags": {
                "log_level": record.levelname,
                "dag_date": record.dag_date,
                "stage_name": record.stage_name
                     },
             "fields": {'message': self.format(record)},
            }
        self.connector.client.write_points([log_json])