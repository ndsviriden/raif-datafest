FROM apache/airflow:2.0.2

USER root
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
         libpq-dev postgresql gcc python-dev python3-dev \
  && apt-get autoremove -yqq --purge \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

USER airflow
ADD requirements.txt .
RUN pip install --no-cache-dir --user -r requirements.txt
